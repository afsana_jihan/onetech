<?php
$data['heading']="All Institute"
?>
@extends('admin.layouts.master',$data)
@section('content')

            <div class="page-content-wrapper">

                <div class="container-fluid">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card m-b-20">
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <table class="table table-hover mb-0">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Type</th>
                                                <th>Country</th>
                                                <th>Code</th>
                                                <th>Email</th>
                                                <th>Contact No</th>
                                                <th>Responsible Person</th>
                                                <th>Contact Person</th>
                                                <th>Profile</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            @foreach($institutes as $row)
                                            <tbody>
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$row->institute_title}}</td>
                                                <td>{{$row->institute_type->institutes_type}}</td>

                                                <td>{{$row->country->country_name}}</td>
                                                <td>{{$row->code}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->contact_no}}</td>
                                                <td>{{$row->responsible_person}}</td>
                                                <td>{{$row->contact_person}}</td>
                                                <td> <img src="{{ URL::to('/') }}/images/{{ $row->profile }}" class="img-thumbnail"></td>
                                                <td><a href="{{route('institute.edit',$row->id)}}" class="btn btn-info"><i class="fa fa-edit"></i></a></td>
                                                <td>
                                                    <form method="POST" action="{{route('institute.destroy',$row->id)}}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div><!-- container -->

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->

        <footer class="footer">
            © 2017 - 2019 Admiria <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand</span>
        </footer>

    </div>
    <!-- End Right content here -->

</div>
<!-- END wrapper -->

    @endsection
