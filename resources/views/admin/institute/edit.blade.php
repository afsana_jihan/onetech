<?php
$data['heading']="Edit Institute"
?>
@extends('admin.layouts.master',$data)

@section('content')
    <div class="page-content-wrapper">

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-12">
                            <div class="card m-b-20">
                                <div class="card-body">
                                    <form action="{{route('institute.update', $institute->id)}}" method="Post" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="_method" value="PATCH" />

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Institute Title </label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" name="institute_title" value="{{$institute->institute_title}}" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Institute Type</label>
                                            <div class="col-sm-10">

                                                <select class="custom-select" name="institute_type_id">
                                                    <option value="">Please choose One Institute Type</option>
                                                    @foreach($types as $row)
                                                        <option value="{{$row->id}}"@if($institute->institute_type_id==$row->id) selected @endif>{{$row->institutes_type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Country</label>
                                            <div class="col-sm-10">

                                                <select class="custom-select" name="country_id">
                                                    @foreach($countries as $row)
                                                        <option value="{{$row->id}}" @if($institute->country_id==$row->id) selected @endif>{{$row->country_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Code</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" name="code" value="{{$institute->code}}" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Email </label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="email" name="email"  value="{{$institute->email}}" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Contact No </label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="tel" name="contact_no"  value="{{$institute->contact_no}}" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Responsible Person </label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" name="responsible_person"   value="{{$institute->responsible_person}}" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Contact Person</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" name="contact_person"  value="{{$institute->contact_person}}" id="example-text-input">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Document </label>

                                            <div class="col-sm-10">
                                                <input type="file" name="document"/>
                                                <a target="_blank"  href="{{ URL::to('/') }}/uploads/{{ $institute->document }}" >click here to see the document</a>
{{--                                                <embed width="191" height="207" name="document" src="{{ URL::to('/') }}/uploads/{{ $institute->document }}" type="application/pdf">--}}
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Profile </label>
                                            <div class="col-sm-10">
                                                <input type="file" name="profile"/>
                                                <img src="{{ URL::to('/') }}/images/{{ $institute->profile }}" class="img-thumbnail" width="190" />

                                            </div>

                                            </div>



                                        <div class="form-group row">
                                            <div class="col-lg-11">
                                            </div>
                                            <div class="col-lg-1">
                                                <input class="btn btn-purple text-right" type="submit" value="Submit">
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->

                </div><!-- container -->

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->

        <footer class="footer">
            © 2017 - 2019 Admiria <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand</span>
        </footer>

    </div>
@endsection

