<div class="left side-menu">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="">
            <!--<a href="index.html" class="logo text-center">Admiria</a>-->
            <a href="index.html" class="logo"><img src="{{asset('admin/images/logo-sm.png')}}" height="36" alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>

                <li class="menu-title">
                    PROJECT UTILIZATION</li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class=" mdi mdi mdi-archive"></i><span>Manage Researcher <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="list-unstyled">
                        <li><a href="">Add Researcher</a></li>
                        <li><a href="">Researcher List</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-pie"></i><span> Manage Fund Level <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="list-unstyled">
                        <li><a href="">Add Fund Level</a></li>
                        <li><a href="">Fund Level List</a></li>
                        <li><a href="">Add Fund Type</a></li>
                        <li><a href="">Fund Type List</a></li>
                    </ul>
                </li>





                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-calculator"></i><span>Manage Institute <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="list-unstyled">
                        <li><a href="">Add Institute</a></li>
                        <li><a href="">Institute List</a></li>
                        <li><a href="">Add Institute Type</a></li>
                        <li><a href="">Institute Type List</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file"></i><span>  Manage Department <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="list-unstyled">
                        <li><a href="">Add Department</a></li>
                        <li><a href=">Department List</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class=" mdi mdi-sitemap"></i><span>Manage Degree <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="list-unstyled">
                        <li><a href="">Add Degree</a></li>
                        <li><a href="">Degree List</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class=" mdi mdi-sitemap"></i><span>Manage Subject <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="list-unstyled">
                        <li><a href="">Add Subject</a></li>
                        <li><a href="">Subject List</a></li>

                    </ul>
                </li>

{{--                <li>--}}
{{--                    <a href="calendar.html" class="waves-effect"><i class=" mdi mdi-trophy"></i><span> Award & Exhibition </span></a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="calendar.html" class="waves-effect"><i class=" mdi mdi-certificate"></i><span> IP & Commercialization </span></a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="calendar.html" class="waves-effect"><i class=" mdi mdi-gavel"></i><span> Asset Management</span></a>--}}
{{--                </li>--}}












            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
