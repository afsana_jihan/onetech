@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/slick-1.8.0/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/styles/product_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/styles/product_responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/order_details.css') }}">
@endpush
@extends('layouts.master')
@section('content')

<br>
<br>
<br>
<br>

<div class="table-wrapper">

        <div class="table-row table-header">
            <div class="col-wrapper order-date-number-po">
                <div class="table-col order-date">#</div>
                <div class="table-col order-number">Product Name</div>
            </div>

            <div class="col-wrapper order-year-model-customer">
                <div class="col-wrapper order-year-model">
                    <div class="table-col order-year">Quantity</div>
{{--                    <div class="table-col order-model"></div>--}}
                </div>
{{--                <div class="table-col order-customer"></div>--}}
            </div>

            <div class="col-wrapper order-status-signed">
                <div class="table-col order-status">Unit Price</div>
                <div class="table-col order-signed">Total</div>
            </div>



        </div>

        <div class="table-row">
            <div class="col-wrapper order-date-number-po">
                <div class="table-col order-date">1</div>
                <div class="table-col order-number">SO-22204</div>
            </div>

            <div class="col-wrapper order-year-model-customer">
                <div class="col-wrapper order-year-model">
                    <div class="table-col order-year">2</div>
                    <div class="table-col order-model"></div>
                </div>
            </div>

            <div class="col-wrapper order-status-signed">
                <div class="table-col order-status">123</div>
                <div class="table-col order-signed">123</div>
            </div>



        </div>

        <div class="table-row">
            <div class="col-wrapper order-date-number-po">
                <div class="table-col order-date">2</div>
                <div class="table-col order-number">SO-21904</div>
            </div>

            <div class="col-wrapper order-year-model-customer">
                <div class="col-wrapper order-year-model">
                    <div class="table-col order-year">2</div>
                    <div class="table-col order-model"></div>
                </div>

            </div>

            <div class="col-wrapper order-status-signed">
                <div class="table-col order-status">123</div>
                <div class="table-col order-signed">123</div>
            </div>



        </div>
        <br>

        <div class="row">
            <div class="offset-md-10 col-md-2">
                <button type="submit" class="btn btn-info">Checkout</button>
            </div>
        </div>

    </div>

<br>
<br>
<br>
<br>
@endsection
@push('scripts')

    <script src="{{ asset('front/plugins/slick-1.8.0/slick.js') }}"></script>
    <script src="{{ asset('front/js/custom.js') }}"></script>

@endpush
