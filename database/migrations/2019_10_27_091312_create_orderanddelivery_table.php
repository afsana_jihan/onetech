<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderanddeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderanddelivery', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->integer('status');
            $table->string('customer_name');
            $table->string('customer_id');
            $table->string('customer_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderanddelivery');
    }
}
