<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        //
        'title' =>$faker->realText(180),
        'subtitle' =>$faker->realText(100),
        'image' => $faker->realText(100),
    ];
});
