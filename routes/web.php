<?php


Route::get('/index', function () {
    return view('index');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('blog_single', function () {
    return view('blog_single');
});
Route::get('/cart', function () {
    return view('cart');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/product', function () {
    return view('product');
});
Route::get('/regular', function () {
    return view('regular');
});
Route::get('/shop', function () {
    return view('shop');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/register', function () {
    return view('register');
})->name('register');
Route::get('/order_details', function () {
    return view('order_details');
});
//Route::get('/admin',function (){
//    return view ('admin.layouts.master');
//});
Route::get('/', function () {
    return view('admin.pages.dashboard');
});



